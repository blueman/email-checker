# Simple Email Checker

Super simple web email validator.

![Application screen](./build/assets/screen.png)

## Options:
- Check email format
When the checkbox is checked then email address will be validated against [egulias/email-validator](https://packagist.org/packages/egulias/email-validator) to perform an RFC compliant validation.
- Check email domain
If checked, then the [checkdnsrr](http://php.net/manual/en/function.checkdnsrr.php) PHP function will be used to check the validity of the MX record of the host of the given email.

### Project builds:
http://blueman.gitlab.io/email-checker/
1. PhpMetrics - http://blueman.gitlab.io/email-checker/phpmetrics/
2. PhpUnit Coverage - http://blueman.gitlab.io/email-checker/phpunit/
