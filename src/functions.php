<?php
declare(strict_types=1);

/**
 * Get minimum value, but except null values.
 *
 * @param mixed $value,...
 *
 * @return mixed
 */
function min_not_null() : int
{
    $values = func_get_args();

    return min(array_filter($values));
}

/**
 * Get maximum value, but except null values.
 *
 * @param mixed $value,...
 *
 * @return mixed
 */
function max_not_null() : int
{
    $values = func_get_args();

    return max(array_filter($values));
}

/**
 * Dump values specified in a function
 * and die execution of php code.
 *
 * @return void
 */
function dd() : void
{
    call_user_func_array('var_dump', func_get_args());
    die;
}
