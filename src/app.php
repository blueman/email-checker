<?php
declare(strict_types=1);

require_once __DIR__.'/../vendor/autoload.php';

/**
 * Init the silex application class.
 */
$app = new Silex\Application();

/**
 * Check if the debug option can be enabled to show errors with more details.
 * It is disabled by default.
 */
if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || (isset($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', 'fe80::1', '::1']))
) {
    $app['debug'] = true;
}

/**
 * Twig Service Providers.
 */
$app->register(new Silex\Provider\AssetServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), [
    'twig.path' => __DIR__.'/../views',
]);
/**
 * Validator Service Provider
 */
$app->register(new Silex\Provider\ValidatorServiceProvider());

/**
 * List of routes.
 */
$app->mount('/', new App\Controllers\IndexController());

return $app;
