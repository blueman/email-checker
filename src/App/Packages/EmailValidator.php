<?php
declare(strict_types=1);

namespace App\Packages;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

/**
 * Class EmailValidator.
 *
 * @package App\Packages
 * @author Szymon Bluma <szbluma@gmail.com>
 */
class EmailValidator
{
    /**
     * check if a email format is ok.
     *
     * @var bool
     */
    private $strict = false;

    /**
     * check MX record of a domain.
     *
     * @var bool
     */
    private $checkHost = false;

    /**
     * list of emails.
     *
     * @var array
     */
    private $emails = [];

    /**
     * EmailValidator constructor.
     *
     * @param string|array $emails
     */
    public function __construct($emails)
    {
        $this->emails = is_array($emails)
            ? $emails
            : [$emails];
    }

    /**
     * @param bool $strict
     */
    public function setStrict(bool $strict)
    {
        $this->strict = $strict;
    }

    /**
     * @param bool $checkHost
     */
    public function setCheckHost(bool $checkHost)
    {
        $this->checkHost = $checkHost;
    }

    /**
     * Run the validation.
     *
     * @return array
     */
    public function validate() : array
    {
        $validator = Validation::createValidatorBuilder()->getValidator();

        $correctEmails = [];
        $wrongEmails = [];

        foreach ($this->emails as $email) {
            $errors = $validator->validate($email, new Assert\Email([
                'strict' => $this->strict,
                'checkHost' => $this->checkHost,
            ]));

            if (count($errors) == 0) {
                $correctEmails[] = $email;
            } else {
                $wrongEmails[] = $email;
            }
        }

        return [$correctEmails, $wrongEmails];
    }
}
