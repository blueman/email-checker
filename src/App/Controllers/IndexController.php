<?php
declare(strict_types=1);

namespace App\Controllers;

use App\Packages\EmailValidator;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\Api\ControllerProviderInterface;

/**
 * Class IndexController.
 *
 * @package App\Controllers
 * @author Szymon Bluma <szbluma@gmail.com>
 */
class IndexController implements ControllerProviderInterface
{
    /**
     * @param Application $app
     *
     * @return ControllerCollection
     */
    public function connect(Application $app) : ControllerCollection
    {
        /**
         * @var ControllerCollection $controllers
         */
        $controllers = $app['controllers_factory'];

        /**
         * Basic routes for the controller.
         */
        $controllers->match('/', 'App\Controllers\IndexController::indexAction')->method('GET|POST');

        return $controllers;
    }

    /**
     * Main action where are the magic is happening.
     *
     * @param Application $app
     * @param Request $request
     *
     * @return string
     */
    public function indexAction(Application $app, Request $request) : string
    {
        $correctEmails = [];
        $wrongEmails = [];

        if ($request->isMethod('post')) {
            /**
             * We have to split an emails line-by-line from textarea
             */
            $emails = array_filter(explode("\r\n", $request->get('email', '')));

            $emailValidator = new EmailValidator($emails);
            $emailValidator->setStrict(boolval($request->get('strict', false)));
            $emailValidator->setCheckHost(boolval($request->get('checkHost', false)));

            list($correctEmails, $wrongEmails) = $emailValidator->validate();
        }

        return $app['twig']->render('index/index.twig.html', [
            'correctEmails' => $correctEmails,
            'wrongEmails' => $wrongEmails
        ]);
    }
}
