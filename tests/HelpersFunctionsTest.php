<?php

class HelpersFunctionsTest extends PHPUnit_Framework_TestCase
{
    public function testMinNotNull()
    {
        $min = min_not_null(0, 1, -3, null);

        $this->assertEquals(-3, $min);
    }

    public function testMaxNotNull()
    {
        $max = max_not_null(0, 1, -3, null);

        $this->assertEquals(1, $max);
    }
}
