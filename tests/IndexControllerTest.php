<?php

use Silex\WebTestCase;

class IndexControllerTest extends WebTestCase
{
    public function createApplication()
    {
        $app = require __DIR__.'/../src/app.php';

        $app['debug'] = true;
        $app['session.test'] = true;
        unset($app['exception_handler']);

        return $app;
    }

    public function testInitialPage()
    {
        $client = $this->createClient();
        $crawler = $client->request('GET', '/');

        $this->assertTrue($client->getResponse()->isOk());
        $this->assertCount(1, $crawler->filter('form'));
        $this->assertCount(1, $crawler->filter('textarea[name=email]'));
    }

    public function testPostSingleEmailPage()
    {
        $client = $this->createClient();
        $crawler = $client->request('POST', '/', [
            'email' => 'admin@gmail.com',
        ]);

        $this->assertTrue($client->getResponse()->isOk());
        $this->assertFalse(is_bool(strpos($crawler->text(), 'None wrong emails.')));
        $this->assertCount(1, $crawler->filter('form'));
        $this->assertCount(1, $crawler->filter('textarea[name=email]'));

        $crawler = $client->request('POST', '/', [
            'email' => 'admin',
        ]);
        $this->assertTrue($client->getResponse()->isOk());
        $this->assertFalse(is_bool(strpos($crawler->text(), 'None correct emails.')));
    }

    public function testPostMultipleEmailPage()
    {
        $client = $this->createClient();
        $crawler = $client->request('POST', '/', [
            'email' => "admin@gmail.com\r\nadmin@yahoo.com",
        ]);

        $this->assertTrue($client->getResponse()->isOk());
        $this->assertFalse(is_bool(strpos($crawler->text(), 'None wrong emails.')));
    }
}
