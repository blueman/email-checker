<?php

class EmailValidatorTest extends PHPUnit_Framework_TestCase
{
    public function testArrayConstructor()
    {
        $emailValidator = new \App\Packages\EmailValidator(['admin@gmail.com', 'aaaa333@onet.pl', 'a', 'adfadfa+dfadsfad@yahoo.com', 'x@x']);
        list($correctEmails, $wrongEmails) = $emailValidator->validate();

        $this->assertCount(3, $correctEmails);
        $this->assertCount(2, $wrongEmails);
    }

    public function testValidAddress()
    {
        $emailValidator = new \App\Packages\EmailValidator('admin@gmail.com');
        list($correctEmails, $wrongEmails) = $emailValidator->validate();

        $this->assertCount(1, $correctEmails);
        $this->assertCount(0, $wrongEmails);
    }

    public function testInvalidAddress()
    {
        $emailValidator = new \App\Packages\EmailValidator('x');
        list($correctEmails, $wrongEmails) = $emailValidator->validate();

        $this->assertCount(0, $correctEmails);
        $this->assertCount(1, $wrongEmails);
    }

    /**
     * Without the strict mode the email is valid.
     */
    public function testValidStrictAddress()
    {
        $emailValidator = new \App\Packages\EmailValidator('first.last@com');
        $emailValidator->setStrict(true);
        list($correctEmails, $wrongEmails) = $emailValidator->validate();

        $this->assertCount(1, $correctEmails);
        $this->assertCount(0, $wrongEmails);
    }

    public function testCheckHostAddress()
    {
        $emailValidator = new \App\Packages\EmailValidator(['first.last@gmail.com','adfad@knkj2n45k243k523jk5b2k352.pl']);
        $emailValidator->setCheckHost(true);
        list($correctEmails, $wrongEmails) = $emailValidator->validate();

        $this->assertCount(1, $correctEmails);
        $this->assertCount(1, $wrongEmails);
    }
}
